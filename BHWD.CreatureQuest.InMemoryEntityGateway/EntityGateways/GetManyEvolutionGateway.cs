﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetManyEvolutionGateway : IGetManyEntityGateway<IEvolution> {

        private readonly IEntityDataSet<IEvolution> EntityDataSet;

        public GetManyEvolutionGateway(IEntityDataSet<IEvolution> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<IEvolution> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}