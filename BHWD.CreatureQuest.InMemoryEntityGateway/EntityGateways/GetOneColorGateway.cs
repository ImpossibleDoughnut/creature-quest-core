﻿using System;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetOneColorGateway : IGetOneEntityGateway<IColor> {

        private readonly IEntityDataSet<IColor> EntityDataSet;

        public GetOneColorGateway(IEntityDataSet<IColor> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public IColor GetById(Guid id) {
            return this.EntityDataSet.Entities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}