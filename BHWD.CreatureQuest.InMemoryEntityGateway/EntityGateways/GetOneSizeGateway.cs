﻿using System;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetOneSizeGateway : IGetOneEntityGateway<ISize> {

        private readonly IEntityDataSet<ISize> EntityDataSet;

        public GetOneSizeGateway(IEntityDataSet<ISize> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public ISize GetById(Guid id) {
            return this.EntityDataSet.Entities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}