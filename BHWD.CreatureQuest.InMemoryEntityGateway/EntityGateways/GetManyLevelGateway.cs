﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetManyLevelGateway : IGetManyEntityGateway<ILevel> {

        private readonly IEntityDataSet<ILevel> EntityDataSet;

        public GetManyLevelGateway(IEntityDataSet<ILevel> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<ILevel> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}