﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetManySizeGateway : IGetManyEntityGateway<ISize> {

        private readonly IEntityDataSet<ISize> EntityDataSet;

        public GetManySizeGateway(IEntityDataSet<ISize> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<ISize> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}