﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetManyAttackGateway : IGetManyEntityGateway<IAttack> {

        private readonly IEntityDataSet<IAttack> EntityDataSet;

        public GetManyAttackGateway(IEntityDataSet<IAttack> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<IAttack> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}