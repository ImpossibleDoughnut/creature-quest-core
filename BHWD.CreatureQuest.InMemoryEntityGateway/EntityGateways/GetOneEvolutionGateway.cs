﻿using System;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetOneEvolutionGateway : IGetOneEntityGateway<IEvolution> {

        private readonly IEntityDataSet<IEvolution> EntityDataSet;

        public GetOneEvolutionGateway(IEntityDataSet<IEvolution> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public IEvolution GetById(Guid id) {
            return this.EntityDataSet.Entities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}