﻿using System;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetOneCreatureGateway : IGetOneEntityGateway<ICreature> {

        private readonly IEntityDataSet<ICreature> EntityDataSet;

        public GetOneCreatureGateway(IEntityDataSet<ICreature> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public ICreature GetById(Guid id) {
            return this.EntityDataSet.Entities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}