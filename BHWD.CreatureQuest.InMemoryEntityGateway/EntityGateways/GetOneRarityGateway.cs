﻿using System;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways {

    public class GetOneRarityGateway : IGetOneEntityGateway<IRarity> {

        private readonly IEntityDataSet<IRarity> EntityDataSet;

        public GetOneRarityGateway(IEntityDataSet<IRarity> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public IRarity GetById(Guid id) {
            return this.EntityDataSet.Entities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}