﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet {

    public interface IEntityDataSet<TEntity> where TEntity : IEntity {

        List<TEntity> Entities { get; set; }

    }

}