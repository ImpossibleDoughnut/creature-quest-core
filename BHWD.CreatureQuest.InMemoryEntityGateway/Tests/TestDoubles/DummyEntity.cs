﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles {

    internal interface IDummyEntity : IEntity {

    }

    internal class DummyEntity : BaseEntity, IDummyEntity {

    }

}