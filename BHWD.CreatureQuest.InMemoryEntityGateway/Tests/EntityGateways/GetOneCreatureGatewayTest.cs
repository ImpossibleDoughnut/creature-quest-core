﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetOneCreatureGatewayTest {

        private List<ICreature> Creatures { get; set; }
        private IGetOneEntityGateway<ICreature> GetOneCreatureGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Creatures = new List<ICreature> {
                new Creature {
                    Id = Guid.NewGuid(),
                    Name = "Mr Creature"
                },
                new Creature {
                    Id = Guid.NewGuid(),
                    Name = "Mrs Creature"
                }
            };
            var entityDataSet = new StubEntityDataSet<ICreature>(this.Creatures);

            this.GetOneCreatureGateway = new GetOneCreatureGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var expected = this.Creatures[0];
            var actual = this.GetOneCreatureGateway.GetById(expected.Id);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}