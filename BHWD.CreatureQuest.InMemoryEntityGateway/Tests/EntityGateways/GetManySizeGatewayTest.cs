﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetManySizeGatewayTest {

        private List<ISize> Sizes { get; set; }
        private IGetManyEntityGateway<ISize> GetManySizeGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Sizes = new List<ISize> {
                new Size(),
                new Size(),
                new Size()
            };
            var entityDataSet = new StubEntityDataSet<ISize>(this.Sizes);

            this.GetManySizeGateway = new GetManySizeGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetManySizeGateway.GetAll().Count;
            var expected = this.Sizes.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}