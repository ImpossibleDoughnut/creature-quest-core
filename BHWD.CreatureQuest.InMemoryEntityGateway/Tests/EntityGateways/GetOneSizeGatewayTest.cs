﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetOneSizeGatewayTest {

        private List<ISize> Sizes { get; set; }
        private IGetOneEntityGateway<ISize> GetOneSizeGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Sizes = new List<ISize> {
                new Size {
                    Id = Guid.NewGuid(),
                    Name = "Mr Size"
                },
                new Size {
                    Id = Guid.NewGuid(),
                    Name = "Mrs Size"
                }
            };
            var entityDataSet = new StubEntityDataSet<ISize>(this.Sizes);

            this.GetOneSizeGateway = new GetOneSizeGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var expected = this.Sizes[0];
            var actual = this.GetOneSizeGateway.GetById(expected.Id);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}