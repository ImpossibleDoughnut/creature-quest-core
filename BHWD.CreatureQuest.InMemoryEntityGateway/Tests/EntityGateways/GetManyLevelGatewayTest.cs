﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetManyLevelGatewayTest {

        private List<ILevel> Levels { get; set; }
        private IGetManyEntityGateway<ILevel> GetManyLevelGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Levels = new List<ILevel> {
                new Level(),
                new Level(),
                new Level()
            };
            var entityDataSet = new StubEntityDataSet<ILevel>(this.Levels);

            this.GetManyLevelGateway = new GetManyLevelGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetManyLevelGateway.GetAll().Count;
            var expected = this.Levels.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}