﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class GetOneAttackGatewayTest {

        private List<IAttack> Attacks { get; set; }
        private IGetOneEntityGateway<IAttack> GetOneAttackGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Attacks = new List<IAttack> {
                new Attack {
                    Id = Guid.NewGuid(),
                    Name = "Mr Attack"
                },
                new Attack {
                    Id = Guid.NewGuid(),
                    Name = "Mrs Attack"
                }
            };
            var entityDataSet = new StubEntityDataSet<IAttack>(this.Attacks);

            this.GetOneAttackGateway = new GetOneAttackGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var expected = this.Attacks[0];
            var actual = this.GetOneAttackGateway.GetById(expected.Id);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}