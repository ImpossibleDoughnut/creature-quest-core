﻿using System;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;
using BHWD.CreatureQuest.InMemoryEntityGateway.EntityGateways;
using BHWD.CreatureQuest.InMemoryEntityGateway.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.Tests.EntityGateways {

    [TestFixture]
    internal class CreateCreatureGatewayTest {

        private IEntityDataSet<ICreature> EntityDataSet { get; set; }
        private ICreateEntityGateway<ICreature> CreateCreatureGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.EntityDataSet = new StubEntityDataSet<ICreature>();
            this.CreateCreatureGateway = new CreateCreatureGateway(this.EntityDataSet);
        }

        [Test]
        public void AppendsId() {
            var creature = this.CreateCreatureGateway.Create(new Creature());
            var actual = true;
            const bool expected = true;

            if (creature.Id.Equals(Guid.Empty)) {
                actual = false;
            }

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void AppendsToDataSet() {
            this.CreateCreatureGateway.Create(new Creature());
            this.CreateCreatureGateway.Create(new Creature());
            this.CreateCreatureGateway.Create(new Creature());
            var actual = this.EntityDataSet.Entities.Count;
            const int expected = 3;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ThrowsExceptionIfEntityIsNull() {
            Assert.Throws<ArgumentNullException>(() => this.CreateCreatureGateway.Create(null));
        }

        [Test]
        public void ThrowsExceptionIfIdIsSet() {
            var entity = new Creature {
                Id = Guid.NewGuid()
            };

            Assert.Throws<InvalidOperationException>(() => this.CreateCreatureGateway.Create(entity));
        }

    }

}