﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.InMemoryEntityGateway.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.InMemoryEntityGateway.EntityDataSets {

    public class EntityDataSet<TEntity> : IEntityDataSet<TEntity> where TEntity : IEntity {

        public List<TEntity> Entities { get; set; }

        public EntityDataSet() {
            this.Entities = new List<TEntity>();
        }

    }

}