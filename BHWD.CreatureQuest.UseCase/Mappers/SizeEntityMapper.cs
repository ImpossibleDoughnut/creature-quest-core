﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public class SizeEntityMapper : BaseEntityMapper<ISize, Dictionary<string, object>> {

        public const string OutputKeyName = "name";

        public override void MapOne(ISize input, Dictionary<string, object> output) {
            output.Add(OutputKeyId, input.Id);
            output.Add(OutputKeyName, input.Name);
        }

    }

}