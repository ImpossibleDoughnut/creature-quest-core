﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;

namespace BHWD.CreatureQuest.UseCase.Mappers {

    public class EvolutionEntityMapper : BaseEntityMapper<IEvolution, Dictionary<string, object>> {

        private readonly IEntityMapper<IRarity, Dictionary<string, object>> RarityEntityMapper;
        private readonly IEntityMapper<ISize, Dictionary<string, object>> SizeEntityMapper;

        public const string OutputKeyCardinality = "cardinality";
        public const string OutputKeyRarities = "rarities";
        public const string OutputKeySizes = "sizes";

        public EvolutionEntityMapper(IEntityMapper<IRarity, Dictionary<string, object>> rarityEntityMapper, IEntityMapper<ISize, Dictionary<string, object>> sizeEntityMapper) {
            this.RarityEntityMapper = rarityEntityMapper;
            this.SizeEntityMapper = sizeEntityMapper;
        }

        public override void MapOne(IEvolution input, Dictionary<string, object> output) {
            output.Add(OutputKeyId, input.Id);
            output.Add(OutputKeyCardinality, input.Cardinality);

            this.MapRarity(input, output);
            this.MapSize(input, output);
        }

        private void MapRarity(IEvolution input, IDictionary<string, object> output) {
            var rarities = new List<Dictionary<string, object>>();

            if (input.Rarities != null) {
                this.RarityEntityMapper.MapMany(input.Rarities, rarities);
            }

            output.Add(OutputKeyRarities, rarities);
        }

        private void MapSize(IEvolution input, IDictionary<string, object> output) {
            var sizes = new List<Dictionary<string, object>>();

            if (input.Sizes != null) {
                this.SizeEntityMapper.MapMany(input.Sizes, sizes);
            }

            output.Add(OutputKeySizes, sizes);
        }

    }

}