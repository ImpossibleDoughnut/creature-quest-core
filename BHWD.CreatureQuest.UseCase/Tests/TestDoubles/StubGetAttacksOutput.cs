﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetAttacksOutput : IGetAttacksOutput {

        public List<Dictionary<string, object>> Attacks { get; set; }

        public StubGetAttacksOutput() {
            this.Attacks = new List<Dictionary<string, object>>();
        }

    }

}