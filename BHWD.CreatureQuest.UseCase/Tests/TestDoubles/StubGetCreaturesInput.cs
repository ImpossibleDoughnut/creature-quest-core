﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetCreaturesInput : IGetCreaturesInput {

        public List<Guid> ColorIds { get; set; }
        public List<Guid> EvolutionIds { get; set; }
        public List<Guid> SizeIds { get; set; }

        public StubGetCreaturesInput() {
            this.ColorIds = new List<Guid>();
            this.EvolutionIds = new List<Guid>();
            this.SizeIds = new List<Guid>();
        }

    }

}