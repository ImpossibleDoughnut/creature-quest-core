﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetEvolutionsInput : IGetEvolutionsInput {

        public List<Guid> RarityIds { get; set; }
        public List<Guid> SizeIds { get; set; }

        public StubGetEvolutionsInput() {
            this.RarityIds = new List<Guid>();
            this.SizeIds = new List<Guid>();
        }

    }

}