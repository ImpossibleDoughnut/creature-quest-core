﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal interface IDummyUseCaseInput : IUseCaseInput {

    }

    internal class DummyUseCaseInput : IDummyUseCaseInput {

    }

}