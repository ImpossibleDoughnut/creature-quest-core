﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetRaritiesOutput : IGetRaritiesOutput {

        public List<Dictionary<string, object>> Rarities { get; set; }

        public StubGetRaritiesOutput() {
            this.Rarities = new List<Dictionary<string, object>>();
        }

    }

}