﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetEvolutionGateway : IGetManyEntityGateway<IEvolution>, IGetOneEntityGateway<IEvolution> {

        private readonly List<IEvolution> Evolutions;

        public StubGetEvolutionGateway(List<IEvolution> evolutions) {
            this.Evolutions = evolutions;
        }

        public List<IEvolution> GetAll() {
            return this.Evolutions;
        }

        public IEvolution GetById(Guid id) {
            return this.Evolutions.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}