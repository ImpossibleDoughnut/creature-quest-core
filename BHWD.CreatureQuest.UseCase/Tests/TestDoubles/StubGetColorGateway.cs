﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetColorGateway : IGetManyEntityGateway<IColor>, IGetOneEntityGateway<IColor> {

        private readonly List<IColor> Colors;

        public StubGetColorGateway(List<IColor> colors) {
            this.Colors = colors;
        }

        public List<IColor> GetAll() {
            return this.Colors;
        }

        public IColor GetById(Guid id) {
            return this.Colors.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}