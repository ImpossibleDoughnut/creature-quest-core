﻿using System;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetLevelsInput : IGetLevelsInput {

        public Guid EvolutionId { get; set; }
        public Guid SizeId { get; set; }

    }

}