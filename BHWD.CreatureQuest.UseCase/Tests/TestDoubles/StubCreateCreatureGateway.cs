﻿using System;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubCreateCreatureGateway : ICreateEntityGateway<ICreature> {

        public ICreature Create(ICreature creature) {
            creature.Id = Guid.NewGuid();

            return creature;
        }

    }

}