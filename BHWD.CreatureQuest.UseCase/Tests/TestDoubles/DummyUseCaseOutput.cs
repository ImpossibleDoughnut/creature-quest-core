﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal interface IDummyUseCaseOutput : IUseCaseOutput {

    }

    internal class DummyUseCaseOutput : IDummyUseCaseOutput {

    }

}