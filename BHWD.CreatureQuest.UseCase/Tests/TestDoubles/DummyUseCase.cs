﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal interface IDummyUseCase : IUseCase<IDummyUseCaseInput, IDummyUseCaseOutput> {

    }

    internal class DummyUseCase : IDummyUseCase {

        public void Execute(IDummyUseCaseInput input, IDummyUseCaseOutput output) {
        }

    }

}