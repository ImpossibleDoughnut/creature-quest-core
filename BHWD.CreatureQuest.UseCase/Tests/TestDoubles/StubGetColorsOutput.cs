﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetColorsOutput : IGetColorsOutput {

        public List<Dictionary<string, object>> Colors { get; set; }

        public StubGetColorsOutput() {
            this.Colors = new List<Dictionary<string, object>>();
        }

    }

}