﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetRarityGateway : IGetManyEntityGateway<IRarity>, IGetOneEntityGateway<IRarity> {

        private readonly List<IRarity> Rarities;

        public StubGetRarityGateway(List<IRarity> rarities) {
            this.Rarities = rarities;
        }

        public List<IRarity> GetAll() {
            return this.Rarities;
        }

        public IRarity GetById(Guid id) {
            return this.Rarities.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}