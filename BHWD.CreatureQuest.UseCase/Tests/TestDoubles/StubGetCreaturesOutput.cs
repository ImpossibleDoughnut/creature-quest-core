﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetCreaturesOutput : IGetCreaturesOutput {

        public List<Dictionary<string, object>> Creatures { get; set; }

        public StubGetCreaturesOutput() {
            this.Creatures = new List<Dictionary<string, object>>();
        }

    }

}