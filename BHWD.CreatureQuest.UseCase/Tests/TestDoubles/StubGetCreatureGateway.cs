﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetCreatureGateway : IGetManyEntityGateway<ICreature>, IGetOneEntityGateway<ICreature> {

        private readonly List<ICreature> Creatures;

        public StubGetCreatureGateway(List<ICreature> creatures) {
            this.Creatures = creatures;
        }

        public List<ICreature> GetAll() {
            return this.Creatures;
        }

        public ICreature GetById(Guid id) {
            return this.Creatures.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}