﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.UseCase.Tests.TestDoubles {

    internal class StubGetLevelGateway : IGetManyEntityGateway<ILevel>, IGetOneEntityGateway<ILevel> {

        private readonly List<ILevel> Levels;

        public StubGetLevelGateway(List<ILevel> levels) {
            this.Levels = levels;
        }

        public List<ILevel> GetAll() {
            return this.Levels;
        }

        public ILevel GetById(Guid id) {
            return this.Levels.FirstOrDefault(entity => id.Equals(entity.Id));
        }

    }

}