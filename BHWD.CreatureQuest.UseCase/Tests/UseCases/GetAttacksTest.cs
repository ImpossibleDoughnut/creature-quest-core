﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class GetAttacksTest {

        private IGetAttacks GetAttacks { get; set; }
        private List<IAttack> Attacks { get; set; }
        private IGetAttacksInput GetAttacksInput { get; set; }
        private IGetAttacksOutput GetAttacksOutput { get; set; }

        [SetUp]
        public void SetUp() {
            var attackMapper = new AttackEntityMapper();

            this.Attacks = new List<IAttack> {
                new Attack(),
                new Attack(),
                new Attack()
            };
            this.GetAttacks = new GetAttacks(new StubGetAttackGateway(this.Attacks), attackMapper);
            this.GetAttacksInput = new StubGetAttacksInput();
            this.GetAttacksOutput = new StubGetAttacksOutput();

            this.GetAttacks.Execute(this.GetAttacksInput, this.GetAttacksOutput);
        }

        [Test]
        public void OutputsAttacks() {
            var actual = this.GetAttacksOutput.Attacks.Count;
            var exepcted = this.Attacks.Count;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

    }

}