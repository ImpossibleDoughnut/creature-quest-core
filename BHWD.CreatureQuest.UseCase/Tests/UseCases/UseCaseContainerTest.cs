﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class UseCaseContainerTest {

        private IUseCaseContainer UseCaseContainer { get; set; }

        [SetUp]
        public void SetUp() {
            this.UseCaseContainer = new UseCaseContainer();
        }

        [Test]
        public void Instances() {
            var actual = UseCase.UseCases.UseCaseContainer.Instance;
            var expected = UseCase.UseCases.UseCaseContainer.Instance;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Registers() {
            var actual = this.UseCaseContainer.Register<IDummyUseCase, IDummyUseCaseInput, IDummyUseCaseOutput>(new DummyUseCase());
            const bool expected = true;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Gets() {
            this.UseCaseContainer.Register<IDummyUseCase, IDummyUseCaseInput, IDummyUseCaseOutput>(new DummyUseCase());
            var actual = this.UseCaseContainer.Get<IDummyUseCase, IDummyUseCaseInput, IDummyUseCaseOutput>();

            Assert.IsNotNull(actual);
        }

        [Test]
        public void TypesAreRetained() {
            var useCase = new DummyUseCase();
            this.UseCaseContainer.Register<IDummyUseCase, IDummyUseCaseInput, IDummyUseCaseOutput>(useCase);
            var actual = this.UseCaseContainer.Get<IDummyUseCase, IDummyUseCaseInput, IDummyUseCaseOutput>().GetType();
            var expected = useCase.GetType();

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}