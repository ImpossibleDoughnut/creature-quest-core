﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class GetEvolutionsTest {

        private IGetEvolutions GetEvolutions { get; set; }
        private List<IEvolution> Evolutions { get; set; }

        private readonly IRarity Common = new Rarity {
            Id = Guid.NewGuid(),
            Name = "Common"
        };

        private readonly IRarity Rare = new Rarity {
            Id = Guid.NewGuid(),
            Name = "Rare"
        };

        private readonly ISize Small = new Size {
            Id = Guid.NewGuid(),
            Name = "Small"
        };

        private readonly ISize Medium = new Size {
            Id = Guid.NewGuid(),
            Name = "Medium"
        };

        [SetUp]
        public void SetUp() {
            var evolutionMapper = new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper());

            this.Evolutions = new List<IEvolution> {
                new Evolution {
                    Rarities = new List<IRarity> {
                        this.Common
                    },
                    Sizes = new List<ISize> {
                        this.Small
                    }
                },
                new Evolution {
                    Rarities = new List<IRarity> {
                        this.Rare
                    },
                    Sizes = new List<ISize> {
                        this.Medium
                    }
                },
                new Evolution {
                    Rarities = new List<IRarity> {
                        this.Common,
                        this.Rare
                    },
                    Sizes = new List<ISize> {
                        this.Small,
                        this.Medium
                    }
                },
                new Evolution()
            };
            this.GetEvolutions = new GetEvolutions(new StubGetEvolutionGateway(this.Evolutions), evolutionMapper);
        }

        [Test]
        public void OutputsEvolutionsWithoutFilter() {
            var input = new StubGetEvolutionsInput();
            var output = new StubGetEvolutionsOutput();

            this.GetEvolutions.Execute(input, output);

            var actual = output.Evolutions.Count;
            var exepcted = this.Evolutions.Count;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEvolutionsWithSingleRarityFilter() {
            var input = new StubGetEvolutionsInput();
            var output = new StubGetEvolutionsOutput();
            var rarityFilters = new List<Guid> {
                this.Common.Id
            };

            input.RarityIds.AddRange(rarityFilters);
            this.GetEvolutions.Execute(input, output);

            var actual = output.Evolutions.Count;
            const int exepcted = 2;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEvolutionsWithSingleSizeFilter() {
            var input = new StubGetEvolutionsInput();
            var output = new StubGetEvolutionsOutput();
            var sizeFilters = new List<Guid> {
                this.Small.Id
            };

            input.SizeIds.AddRange(sizeFilters);
            this.GetEvolutions.Execute(input, output);

            var actual = output.Evolutions.Count;
            const int exepcted = 2;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEvolutionsWithMultipleRarityFilters() {
            var input = new StubGetEvolutionsInput();
            var output = new StubGetEvolutionsOutput();
            var rarityFilters = new List<Guid> {
                this.Common.Id,
                this.Rare.Id
            };

            input.RarityIds.AddRange(rarityFilters);
            this.GetEvolutions.Execute(input, output);

            var actual = output.Evolutions.Count;
            const int exepcted = 3;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEvolutionsWithMultipleSizeFilters() {
            var input = new StubGetEvolutionsInput();
            var output = new StubGetEvolutionsOutput();
            var sizeFilters = new List<Guid> {
                this.Small.Id,
                this.Medium.Id
            };

            input.SizeIds.AddRange(sizeFilters);
            this.GetEvolutions.Execute(input, output);

            var actual = output.Evolutions.Count;
            const int exepcted = 3;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

        [Test]
        public void OutputsEvolutionsWithMultipleMixedFilters() {
            var input = new StubGetEvolutionsInput();
            var output = new StubGetEvolutionsOutput();
            var rarityFilters = new List<Guid> {
                this.Rare.Id
            };
            var sizeFilters = new List<Guid> {
                this.Small.Id
            };

            input.RarityIds.AddRange(rarityFilters);
            input.SizeIds.AddRange(sizeFilters);
            this.GetEvolutions.Execute(input, output);

            var actual = output.Evolutions.Count;
            const int exepcted = 3;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

    }

}