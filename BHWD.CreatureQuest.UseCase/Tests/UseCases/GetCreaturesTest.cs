﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.UseCases;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class GetCreaturesTest {

        private Mock<IEntityMapper<ICreature, Dictionary<string, object>>> CreatureMapper;
        private IGetCreatures UseCase;
        private Mock<IGetManyCreatureGateway> EntityGateway;

        [SetUp]
        public void SetUp() {
            this.EntityGateway = new Mock<IGetManyCreatureGateway>();
            this.CreatureMapper = new Mock<IEntityMapper<ICreature, Dictionary<string, object>>>();

            this.UseCase = new GetCreatures(this.EntityGateway.Object, this.CreatureMapper.Object);
        }

        [Test]
        public void InvokesGateway() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();

            this.UseCase.Execute(useCaseInput.Object, useCaseOutput.Object);

            this.EntityGateway.Verify(x => x.GetAll(It.IsAny<List<Guid>>(), It.IsAny<List<Guid>>(), It.IsAny<List<Guid>>()), Times.Once);
        }

        [Test]
        public void InvokesGatewayWithColorIds() {
            var colorIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();

            useCaseInput.SetupGet(x => x.ColorIds).Returns(colorIds);

            this.UseCase.Execute(useCaseInput.Object, useCaseOutput.Object);

            this.EntityGateway.Verify(x => x.GetAll(colorIds, It.IsAny<List<Guid>>(), It.IsAny<List<Guid>>()), Times.Once);
        }

        [Test]
        public void InvokesGatewayWithEvolutionIds() {
            var evolutionIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();

            useCaseInput.SetupGet(x => x.EvolutionIds).Returns(evolutionIds);

            this.UseCase.Execute(useCaseInput.Object, useCaseOutput.Object);

            this.EntityGateway.Verify(x => x.GetAll(It.IsAny<List<Guid>>(), evolutionIds, It.IsAny<List<Guid>>()), Times.Once);
        }

        [Test]
        public void InvokesGatewayWithSizeIds() {
            var sizeIds = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();

            useCaseInput.SetupGet(x => x.SizeIds).Returns(sizeIds);

            this.UseCase.Execute(useCaseInput.Object, useCaseOutput.Object);

            this.EntityGateway.Verify(x => x.GetAll(It.IsAny<List<Guid>>(), It.IsAny<List<Guid>>(), sizeIds), Times.Once);
        }

        [Test]
        public void InvokesMapper() {
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();

            this.UseCase.Execute(useCaseInput.Object, useCaseOutput.Object);

            this.CreatureMapper.Verify(x => x.MapMany(It.IsAny<IEnumerable<ICreature>>(), It.IsAny<List<Dictionary<string, object>>>()), Times.Once);
        }

        [Test]
        public void InvokesMapperWithCreatures() {
            var creatures = new List<ICreature> {
                new Creature(),
                new Creature(),
                new Creature()
            };
            var useCaseInput = new Mock<IGetCreaturesInput>();
            var useCaseOutput = new Mock<IGetCreaturesOutput>();

            this.EntityGateway.Setup(x => x.GetAll(It.IsAny<List<Guid>>(), It.IsAny<List<Guid>>(), It.IsAny<List<Guid>>())).Returns(creatures);
            this.UseCase.Execute(useCaseInput.Object, useCaseOutput.Object);

            this.CreatureMapper.Verify(x => x.MapMany(creatures, It.IsAny<List<Dictionary<string, object>>>()), Times.Once);
        }

    }

}