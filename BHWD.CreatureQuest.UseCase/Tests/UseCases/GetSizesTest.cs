﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.Mappers;
using BHWD.CreatureQuest.UseCase.Tests.TestDoubles;
using BHWD.CreatureQuest.UseCase.UseCases;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.UseCases {

    [TestFixture]
    internal class GetSizesTest {

        private IGetSizes GetSizes { get; set; }
        private List<ISize> Sizes { get; set; }
        private IGetSizesInput GetSizesInput { get; set; }
        private IGetSizesOutput GetSizesOutput { get; set; }

        [SetUp]
        public void SetUp() {
            var sizeMapper = new SizeEntityMapper();

            this.Sizes = new List<ISize> {
                new Size(),
                new Size(),
                new Size()
            };
            this.GetSizes = new GetSizes(new StubGetSizeGateway(this.Sizes), sizeMapper);
            this.GetSizesInput = new StubGetSizesInput();
            this.GetSizesOutput = new StubGetSizesOutput();

            this.GetSizes.Execute(this.GetSizesInput, this.GetSizesOutput);
        }

        [Test]
        public void OutputsSizes() {
            var actual = this.GetSizesOutput.Sizes.Count;
            var exepcted = this.Sizes.Count;

            Assert.That(actual, Is.EqualTo(exepcted));
        }

    }

}