﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class EvolutionMapperTest {

        private List<IEvolution> Evolutions { get; set; }
        private IEntityMapper<IEvolution, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            var rarities = new List<IRarity> {
                new Rarity {
                    Id = Guid.NewGuid(),
                    Name = "Mr Rarity"
                }
            };
            var sizes = new List<ISize> {
                new Size {
                    Id = Guid.NewGuid(),
                    Name = "Mr. Size"
                }
            };
            this.Evolutions = new List<IEvolution> {
                new Evolution {
                    Id = Guid.NewGuid(),
                    Cardinality = 1,
                    Rarities = rarities,
                    Sizes = sizes
                }
            };
            this.EntityMapper = new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper());
        }

        [Test]
        public void MapsId() {
            var output = new Dictionary<string, object>();
            var expected = this.Evolutions.FirstOrDefault()?.Id;

            this.EntityMapper.MapOne(this.Evolutions.FirstOrDefault(), output);

            output.TryGetValue(EvolutionEntityMapper.OutputKeyId, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsCardinality() {
            var output = new Dictionary<string, object>();
            var expected = this.Evolutions.FirstOrDefault()?.Cardinality;

            this.EntityMapper.MapOne(this.Evolutions.FirstOrDefault(), output);

            output.TryGetValue(EvolutionEntityMapper.OutputKeyCardinality, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsRarities() {
            var output = new Dictionary<string, object>();
            var expected = this.Evolutions.FirstOrDefault()?.Rarities.Count;

            this.EntityMapper.MapOne(this.Evolutions.FirstOrDefault(), output);

            output.TryGetValue(EvolutionEntityMapper.OutputKeyRarities, out var actual);

            var actualList = (List<Dictionary<string, object>>) actual;

            Assert.That(actualList?.Count, Is.EqualTo(expected));
        }

        [Test]
        public void MapsSizes() {
            var output = new Dictionary<string, object>();
            var expected = this.Evolutions.FirstOrDefault()?.Sizes.Count;

            this.EntityMapper.MapOne(this.Evolutions.FirstOrDefault(), output);

            output.TryGetValue(EvolutionEntityMapper.OutputKeySizes, out var actual);

            var actualList = (List<Dictionary<string, object>>) actual;

            Assert.That(actualList?.Count, Is.EqualTo(expected));
        }

    }

}