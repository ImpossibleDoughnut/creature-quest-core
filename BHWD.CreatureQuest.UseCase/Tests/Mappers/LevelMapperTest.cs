﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Mappers;
using NUnit.Framework;

namespace BHWD.CreatureQuest.UseCase.Tests.Mappers {

    [TestFixture]
    internal class LevelMapperTest {

        private List<ILevel> Levels { get; set; }
        private IEntityMapper<ILevel, Dictionary<string, object>> EntityMapper { get; set; }

        [SetUp]
        public void SetUp() {
            var levelRequirements = new List<ILevelRequirement> {
                new LevelRequirement {
                    Id = Guid.NewGuid(),
                    Evolution = new Evolution {
                        Id = Guid.NewGuid(),
                        Cardinality = 1
                    },
                    Size = new Size {
                        Id = Guid.NewGuid(),
                        Name = "Mr Size"
                    }
                }
            };
            this.Levels = new List<ILevel> {
                new Level {
                    Id = Guid.NewGuid(),
                    Number = 15,
                    LevelRequirements = levelRequirements
                }
            };
            this.EntityMapper = new LevelEntityMapper(new LevelRequirementEntityMapper(new EvolutionEntityMapper(new RarityEntityMapper(), new SizeEntityMapper()), new SizeEntityMapper()));
        }

        [Test]
        public void MapsId() {
            var output = new Dictionary<string, object>();
            var expected = this.Levels.FirstOrDefault()?.Id;

            this.EntityMapper.MapOne(this.Levels.FirstOrDefault(), output);

            output.TryGetValue(LevelEntityMapper.OutputKeyId, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapsLevelRequirements() {
            var output = new Dictionary<string, object>();
            var expected = this.Levels.FirstOrDefault()?.LevelRequirements.Count;

            this.EntityMapper.MapOne(this.Levels.FirstOrDefault(), output);

            output.TryGetValue(LevelEntityMapper.OutputKeyLevelRequirements, out var actual);

            var actualList = (List<Dictionary<string, object>>) actual;

            Assert.That(actualList?.Count, Is.EqualTo(expected));
        }

        [Test]
        public void MapsNumber() {
            var output = new Dictionary<string, object>();
            var expected = this.Levels.FirstOrDefault()?.Number;

            this.EntityMapper.MapOne(this.Levels.FirstOrDefault(), output);

            output.TryGetValue(LevelEntityMapper.OutputKeyNumber, out var actual);

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}