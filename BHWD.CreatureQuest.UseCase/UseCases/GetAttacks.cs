﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class GetAttacks : IGetAttacks {

        private readonly IGetManyEntityGateway<IAttack> GetManyAttackGateway;
        private readonly IEntityMapper<IAttack, Dictionary<string, object>> AttackEntityMapper;

        public GetAttacks(IGetManyEntityGateway<IAttack> getManyAttackGateway, IEntityMapper<IAttack, Dictionary<string, object>> attackEntityMapper) {
            this.GetManyAttackGateway = getManyAttackGateway;
            this.AttackEntityMapper = attackEntityMapper;
        }

        public void Execute(IGetAttacksInput input, IGetAttacksOutput output) {
            var attacks = this.GetManyAttackGateway.GetAll();

            this.AttackEntityMapper.MapMany(attacks, output.Attacks);
        }

    }

}