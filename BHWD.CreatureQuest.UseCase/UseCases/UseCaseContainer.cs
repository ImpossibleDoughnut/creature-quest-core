﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class UseCaseContainer : IUseCaseContainer {

        public static IUseCaseContainer Instance { get; set; } = new UseCaseContainer();

        private Dictionary<Type, object> UseCases { get; } = new Dictionary<Type, object>();

        public bool Register<TUseCase, TUseCaseInput, TUseCaseOutput>(TUseCase useCase)
            where TUseCase : IUseCase<TUseCaseInput, TUseCaseOutput> where TUseCaseInput : IUseCaseInput where TUseCaseOutput : IUseCaseOutput {
            var type = typeof(TUseCase);
            var registered = true;

            try {
                this.UseCases.Add(type, useCase);
            } catch (Exception exception) {
                Console.WriteLine(exception);
                registered = false;
            }

            return registered;
        }

        public TUseCase Get<TUseCase, TUseCaseInput, TUseCaseOutput>()
            where TUseCase : IUseCase<TUseCaseInput, TUseCaseOutput> where TUseCaseInput : IUseCaseInput where TUseCaseOutput : IUseCaseOutput {
            var type = typeof(TUseCase);

            this.UseCases.TryGetValue(type, out var output);

            return (TUseCase) output;
        }

    }

}