﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class GetSizes : IGetSizes {

        private readonly IGetManyEntityGateway<ISize> GetManySizeGateway;
        private readonly IEntityMapper<ISize, Dictionary<string, object>> SizeEntityMapper;

        public GetSizes(IGetManyEntityGateway<ISize> getManySizeGateway, IEntityMapper<ISize, Dictionary<string, object>> sizeEntityMapper) {
            this.GetManySizeGateway = getManySizeGateway;
            this.SizeEntityMapper = sizeEntityMapper;
        }

        public void Execute(IGetSizesInput input, IGetSizesOutput output) {
            var sizes = this.GetManySizeGateway.GetAll();

            this.SizeEntityMapper.MapMany(sizes, output.Sizes);
        }

    }

}