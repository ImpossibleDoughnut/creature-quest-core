﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class GetRarities : IGetRarities {

        private readonly IGetManyEntityGateway<IRarity> GetManyRarityGateway;
        private readonly IEntityMapper<IRarity, Dictionary<string, object>> RarityEntityMapper;

        public GetRarities(IGetManyEntityGateway<IRarity> getManyRarityGateway, IEntityMapper<IRarity, Dictionary<string, object>> rarityEntityMapper) {
            this.GetManyRarityGateway = getManyRarityGateway;
            this.RarityEntityMapper = rarityEntityMapper;
        }

        public void Execute(IGetRaritiesInput input, IGetRaritiesOutput output) {
            var rarities = this.GetManyRarityGateway.GetAll();

            this.RarityEntityMapper.MapMany(rarities, output.Rarities);
        }

    }

}