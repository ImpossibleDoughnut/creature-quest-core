﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class GetLevels : IGetLevels {

        private readonly IGetManyEntityGateway<ILevel> GetManyLevelGateway;
        private readonly IEntityMapper<ILevel, Dictionary<string, object>> LevelEntityMapper;

        public GetLevels(IGetManyEntityGateway<ILevel> getManyLevelGateway, IEntityMapper<ILevel, Dictionary<string, object>> levelEntityMapper) {
            this.GetManyLevelGateway = getManyLevelGateway;
            this.LevelEntityMapper = levelEntityMapper;
        }

        public void Execute(IGetLevelsInput input, IGetLevelsOutput output) {
            var levels = this.GetManyLevelGateway.GetAll();

            levels = this.FilterLevels(input, levels);

            this.LevelEntityMapper.MapMany(levels, output.Levels);
        }

        private List<ILevel> FilterLevels(IGetLevelsInput input, IEnumerable<ILevel> levels) {
            var result = new List<ILevel>();

            foreach (var level in levels) {
                var addToResult = true;

                if (input.EvolutionId != Guid.Empty) {
                    addToResult = false;

                    foreach (var levelRequirement in level.LevelRequirements) {
                        if (input.EvolutionId.Equals(levelRequirement.Evolution?.Id) && levelRequirement.Size == null) {
                            addToResult = true;
                        } else if (input.EvolutionId.Equals(levelRequirement.Evolution?.Id) && levelRequirement.Size != null && input.SizeId != Guid.Empty
                                   && input.SizeId.Equals(levelRequirement.Size?.Id)) {
                            addToResult = true;
                        }
                    }
                }

                if (addToResult) {
                    result.Add(level);
                }
            }

            return result;
        }

    }

}