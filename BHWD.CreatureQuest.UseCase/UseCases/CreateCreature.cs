﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class CreateCreature : ICreateCreature {

        private readonly ICreateEntityGateway<ICreature> CreateCreatureGateway;
        private readonly IEntityMapper<ICreature, Dictionary<string, object>> CreatureEntityMapper;
        private readonly IEntityFactory EntityFactory;
        private readonly IGetOneEntityGateway<IAttack> GetOneAttackGateway;
        private readonly IGetOneEntityGateway<IColor> GetOneColorGateway;
        private readonly IGetOneEntityGateway<ICreature> GetOneCreatureGateway;
        private readonly IGetOneEntityGateway<IEvolution> GetOneEvolutionGateway;
        private readonly IGetOneEntityGateway<ILevel> GetOneLevelGateway;
        private readonly IGetOneEntityGateway<IRarity> GetOneRarityGateway;
        private readonly IGetOneEntityGateway<ISize> GetOneSizeGateway;

        public CreateCreature(
            ICreateEntityGateway<ICreature> createCreatureGateway,
            IEntityMapper<ICreature, Dictionary<string, object>> creatureEntityMapper,
            IEntityFactory entityFactory,
            IGetOneEntityGateway<IAttack> getOneAttackGateway,
            IGetOneEntityGateway<IColor> getOneColorGateway,
            IGetOneEntityGateway<ICreature> getOneCreatureGateway,
            IGetOneEntityGateway<IEvolution> getOneEvolutionGateway,
            IGetOneEntityGateway<ILevel> getOneLevelGateway,
            IGetOneEntityGateway<IRarity> getOneRarityGateway,
            IGetOneEntityGateway<ISize> getOneSizeGateway) {
            this.CreateCreatureGateway = createCreatureGateway;
            this.CreatureEntityMapper = creatureEntityMapper;
            this.EntityFactory = entityFactory;
            this.GetOneAttackGateway = getOneAttackGateway;
            this.GetOneColorGateway = getOneColorGateway;
            this.GetOneCreatureGateway = getOneCreatureGateway;
            this.GetOneEvolutionGateway = getOneEvolutionGateway;
            this.GetOneLevelGateway = getOneLevelGateway;
            this.GetOneRarityGateway = getOneRarityGateway;
            this.GetOneSizeGateway = getOneSizeGateway;
        }

        public void Execute(ICreateCreatureInput input, ICreateCreatureOutput output) {
            this.ValidateInput(input);

            var creature = this.EntityFactory.Create<ICreature>();

            if (input.AttackId != null) {
                creature.Attack = this.GetOneAttackGateway.GetById((Guid) input.AttackId);
            }

            creature.AttackRating = input.AttackRating;

            if (input.ColorId != null) {
                creature.Color = this.GetOneColorGateway.GetById((Guid) input.ColorId);
            }

            creature.DefenseRating = input.DefenseRating;

            if (input.EvolutionId != null) {
                creature.Evolution = this.GetOneEvolutionGateway.GetById((Guid) input.EvolutionId);
            }

            creature.HealthPoints = input.HealthPoints;

            if (input.LevelId != null) {
                creature.Level = this.GetOneLevelGateway.GetById((Guid) input.LevelId);
            }

            creature.LuckRating = input.LuckRating;
            creature.ManaPool = input.ManaPool;
            creature.Name = input.Name;

            if (input.NextCreatureId != null) {
                creature.NextCreature = this.GetOneCreatureGateway.GetById((Guid) input.NextCreatureId);
            }

            creature.PowerRating = input.PowerRating;

            if (input.PreviousCreatureId != null) {
                creature.PreviousCreature = this.GetOneCreatureGateway.GetById((Guid) input.PreviousCreatureId);
            }

            if (input.RarityId != null) {
                creature.Rarity = this.GetOneRarityGateway.GetById((Guid) input.RarityId);
            }

            if (input.SizeId != null) {
                creature.Size = this.GetOneSizeGateway.GetById((Guid) input.SizeId);
            }

            this.ValidateEntity(creature);

            var savedCreature = this.CreateCreatureGateway.Create(creature);
            var mappedCreature = new Dictionary<string, object>();

            this.CreatureEntityMapper.MapOne(savedCreature, mappedCreature);

            output.Creature = mappedCreature;
        }

        private void ValidateInput(ICreateCreatureInput input) {
            if (input.AttackId == null || input.AttackId.Equals(Guid.Empty)) {
                throw new InvalidOperationException("An `attack` is required");
            }

            if (input.ColorId == null || input.ColorId.Equals(Guid.Empty)) {
                throw new InvalidOperationException("A `color` is required");
            }

            if (input.EvolutionId == null || input.EvolutionId.Equals(Guid.Empty)) {
                throw new InvalidOperationException("A `evolution` is required");
            }

            if (input.LevelId == null || input.LevelId.Equals(Guid.Empty)) {
                throw new InvalidOperationException("A `level` is required");
            }

            if (input.Name == null || input.Name.Equals(string.Empty)) {
                throw new InvalidOperationException("A `name` is required");
            }

            if (input.RarityId == null || input.RarityId.Equals(Guid.Empty)) {
                throw new InvalidOperationException("A `rarity` is required");
            }

            if (input.SizeId == null || input.SizeId.Equals(Guid.Empty)) {
                throw new InvalidOperationException("A `size` is required");
            }
        }

        private void ValidateEntity(ICreature entity) {
            if (entity.Attack == null) {
                throw new InvalidOperationException("The provided `attack` was not found");
            }

            if (entity.Color == null) {
                throw new InvalidOperationException("The provided `color` was not found");
            }

            if (entity.Evolution == null) {
                throw new InvalidOperationException("The provided `evolution` was not found");
            }

            if (entity.Level == null) {
                throw new InvalidOperationException("The provided `level` was not found");
            }

            if (entity.Rarity == null) {
                throw new InvalidOperationException("The provided `rarity` was not found");
            }

            if (entity.Size == null) {
                throw new InvalidOperationException("The provided `size` was not found");
            }
        }

    }

}