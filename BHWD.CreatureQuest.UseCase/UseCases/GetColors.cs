﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.UseCase.Contracts.Mappers;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.UseCase.UseCases {

    public class GetColors : IGetColors {

        private readonly IGetManyEntityGateway<IColor> GetManyColorGateway;
        private readonly IEntityMapper<IColor, Dictionary<string, object>> ColorEntityMapper;

        public GetColors(IGetManyEntityGateway<IColor> getManyColorGateway, IEntityMapper<IColor, Dictionary<string, object>> colorEntityMapper) {
            this.GetManyColorGateway = getManyColorGateway;
            this.ColorEntityMapper = colorEntityMapper;
        }

        public void Execute(IGetColorsInput input, IGetColorsOutput output) {
            var colors = this.GetManyColorGateway.GetAll();

            this.ColorEntityMapper.MapMany(colors, output.Colors);
        }

    }

}