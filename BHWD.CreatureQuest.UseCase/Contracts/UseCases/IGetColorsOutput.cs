﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetColorsOutput : IUseCaseOutput {

        List<Dictionary<string, object>> Colors { get; set; }

    }

}