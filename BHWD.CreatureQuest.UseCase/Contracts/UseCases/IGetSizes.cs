﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetSizes : IUseCase<IGetSizesInput, IGetSizesOutput> {

    }

}