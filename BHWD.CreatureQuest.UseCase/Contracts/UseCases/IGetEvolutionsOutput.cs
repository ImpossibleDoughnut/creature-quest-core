﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetEvolutionsOutput : IUseCaseOutput {

        List<Dictionary<string, object>> Evolutions { get; set; }

    }

}