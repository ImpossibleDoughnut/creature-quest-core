﻿using System;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetLevelsInput : IUseCaseInput {

        Guid EvolutionId { get; set; }
        Guid SizeId { get; set; }

    }

}