﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetCreaturesOutput : IUseCaseOutput {

        List<Dictionary<string, object>> Creatures { get; set; }

    }

}