﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface ICreateCreature : IUseCase<ICreateCreatureInput, ICreateCreatureOutput> {

    }

}