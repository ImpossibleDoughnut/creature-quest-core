﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetColors : IUseCase<IGetColorsInput, IGetColorsOutput> {

    }

}