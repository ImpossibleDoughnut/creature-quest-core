﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface ICreateCreatureOutput : IUseCaseOutput {

        Dictionary<string, object> Creature { get; set; }

    }

}