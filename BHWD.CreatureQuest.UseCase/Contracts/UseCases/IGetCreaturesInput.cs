﻿using System;
using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetCreaturesInput : IUseCaseInput {

        List<Guid> ColorIds { get; set; }
        List<Guid> EvolutionIds { get; set; }
        List<Guid> SizeIds { get; set; }

    }

}