﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetRaritiesOutput : IUseCaseOutput {

        List<Dictionary<string, object>> Rarities { get; set; }

    }

}