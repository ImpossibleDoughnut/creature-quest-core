﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IGetEvolutions : IUseCase<IGetEvolutionsInput, IGetEvolutionsOutput> {

    }

}