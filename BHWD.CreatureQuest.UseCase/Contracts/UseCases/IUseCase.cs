﻿namespace BHWD.CreatureQuest.UseCase.Contracts.UseCases {

    public interface IUseCase<in TInput, in TOutput> where TInput : IUseCaseInput where TOutput : IUseCaseOutput {

        void Execute(TInput input, TOutput output);

    }

}