﻿namespace BHWD.CreatureQuest.Plugin {

    public interface IPlugin {

        string Description { get; }

        void BootstrapModule();

    }

}