﻿using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.EntityGateways;
using BHWD.CreatureQuest.Entity.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.Entity.Tests.EntityGateways {

    [TestFixture]
    internal class EntityGatewayContainerTest {

        private IEntityGatewayContainer EntityGatewayContainer { get; set; }

        [SetUp]
        public void SetUp() {
            this.EntityGatewayContainer = new EntityGatewayContainer();
            Entity.EntityGateways.EntityGatewayContainer.Instance = this.EntityGatewayContainer;
        }

        [Test]
        public void Instances() {
            var actual = Entity.EntityGateways.EntityGatewayContainer.Instance;
            var expected = this.EntityGatewayContainer;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Registers() {
            var actual = this.EntityGatewayContainer.Register<IGetManyEntityGateway<IDummy>>(new DummyGetManyDummyGateway());
            const bool expected = true;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Gets() {
            this.EntityGatewayContainer.Register<IGetManyEntityGateway<IDummy>>(new DummyGetManyDummyGateway());
            var actual = this.EntityGatewayContainer.Get<IGetManyEntityGateway<IDummy>>();

            Assert.IsNotNull(actual);
        }

        [Test]
        public void TypesAreRetained() {
            var entityGateway = new DummyGetManyDummyGateway();
            this.EntityGatewayContainer.Register<IGetManyEntityGateway<IDummy>>(entityGateway);
            var actual = this.EntityGatewayContainer.Get<IGetManyEntityGateway<IDummy>>().GetType();
            var expected = entityGateway.GetType();

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}