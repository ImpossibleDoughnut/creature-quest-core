﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;

namespace BHWD.CreatureQuest.Entity.Tests.TestDoubles {

    internal class DummyGetManyDummyGateway : IGetManyEntityGateway<IDummy> {

        public List<ICreature> Execute() {
            return new List<ICreature>();
        }

        public List<IDummy> GetAll() {
            return new List<IDummy>();
        }

    }

}