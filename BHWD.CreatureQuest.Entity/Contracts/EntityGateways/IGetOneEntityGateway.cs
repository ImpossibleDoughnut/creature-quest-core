﻿using System;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Contracts.EntityGateways {

    public interface IGetOneEntityGateway<out TEntity> : IEntityGateway where TEntity : IEntity {

        TEntity GetById(Guid id);

    }

}