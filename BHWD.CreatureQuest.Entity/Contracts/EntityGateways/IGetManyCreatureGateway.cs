﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Contracts.EntityGateways {

    public interface IGetManyCreatureGateway : IGetManyEntityGateway<ICreature> {

        List<ICreature> GetAll(List<Guid> colorIds, List<Guid> evolutionIds, List<Guid> sizeIds);

    }

}