﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface IEvolution : IEntity {

        int Cardinality { get; set; }
        List<IRarity> Rarities { get; set; }
        List<ISize> Sizes { get; set; }

    }

}