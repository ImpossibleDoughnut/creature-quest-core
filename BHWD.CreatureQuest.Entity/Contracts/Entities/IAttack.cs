﻿namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface IAttack : IEntity {

        string Name { get; set; }

    }

}