﻿namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface ILevelRequirement : IEntity {

        IEvolution Evolution { get; set; }
        ISize Size { get; set; }

    }

}