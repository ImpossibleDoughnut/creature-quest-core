﻿using System;

namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface IEntity {

        Guid Id { get; set; }

        void SetDefaults();

    }

}