﻿namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface ISize : IEntity {

        string Name { get; set; }

    }

}