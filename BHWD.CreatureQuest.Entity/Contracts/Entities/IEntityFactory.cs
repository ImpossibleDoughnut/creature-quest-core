﻿using System;

namespace BHWD.CreatureQuest.Entity.Contracts.Entities {

    public interface IEntityFactory {

        TEntity Create<TEntity>() where TEntity : IEntity;
        bool Register<TEntity>(Type type) where TEntity : IEntity;

    }

}