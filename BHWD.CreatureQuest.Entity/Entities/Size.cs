﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class Size : BaseEntity, ISize {

        public string Name { get; set; }

    }

}