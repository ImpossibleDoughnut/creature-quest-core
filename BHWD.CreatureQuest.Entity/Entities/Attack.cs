﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class Attack : BaseEntity, IAttack {

        public string Name { get; set; }

    }

}