﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class Rarity : BaseEntity, IRarity {

        public string Name { get; set; }

    }

}