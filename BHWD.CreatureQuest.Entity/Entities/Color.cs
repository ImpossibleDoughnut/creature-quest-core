﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class Color : BaseEntity, IColor {

        public string Name { get; set; }

    }

}