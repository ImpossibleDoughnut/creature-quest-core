﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class LevelRequirement : BaseEntity, ILevelRequirement {

        public IEvolution Evolution { get; set; }
        public ISize Size { get; set; }

    }

}