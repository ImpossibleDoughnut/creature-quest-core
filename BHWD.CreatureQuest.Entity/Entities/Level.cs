﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class Level : BaseEntity, ILevel {

        public List<ILevelRequirement> LevelRequirements { get; set; }
        public int Number { get; set; }

        public Level() {
            this.LevelRequirements = new List<ILevelRequirement>();
        }

    }

}