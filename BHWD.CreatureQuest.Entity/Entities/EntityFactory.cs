﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.Entity.Entities {

    public class EntityFactory : IEntityFactory {

        public static IEntityFactory Instance { get; set; } = new EntityFactory();

        private Dictionary<Type, Type> RegisteredEntities { get; } = new Dictionary<Type, Type>();

        public TEntity Create<TEntity>() where TEntity : IEntity {
            this.RegisteredEntities.TryGetValue(typeof(TEntity), out var entity);

            if (entity == null) {
                throw new Exception($"Could not find a type registered for '{typeof(TEntity)}'");
            }

            var result = (TEntity) Activator.CreateInstance(entity);
            result.SetDefaults();

            return result;
        }

        public bool Register<TEntity>(Type type) where TEntity : IEntity {
            var registered = false;

            if (typeof(TEntity).IsAssignableFrom(type)) {
                this.RegisteredEntities.Add(typeof(TEntity), type);
                registered = true;
            }

            return registered;
        }

    }

}