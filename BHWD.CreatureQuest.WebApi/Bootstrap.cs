﻿using System;
using BHWD.CreatureQuest.Plugin;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace BHWD.CreatureQuest.WebApi {

    public class Bootstrap : IPlugin {

        public string Description => "Web API";
        public static IWebHost BuildWebHost(string[] args) => WebHost.CreateDefaultBuilder(args).UseStartup<Startup>().UseUrls("http://0.0.0.0:8080").Build();

        public void BootstrapModule() {
            Console.WriteLine(this.Description + " Starting Up!");
            BuildWebHost(new string[0]).Run();
        }

    }

}