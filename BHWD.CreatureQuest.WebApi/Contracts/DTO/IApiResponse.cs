﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BHWD.CreatureQuest.WebApi.Contracts.DTO {

    public interface IApiResponse {

        Dictionary<string, object> GetResponse(IUseCaseOutput useCaseOutput);
        Dictionary<string, object> GetResponse(IUseCaseOutput useCaseOutput, ModelStateDictionary modelStateDictionary);

    }

}