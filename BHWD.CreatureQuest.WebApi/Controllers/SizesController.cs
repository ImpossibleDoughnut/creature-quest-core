﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BHWD.CreatureQuest.WebApi.Controllers {

    [Route("[controller]")]
    public class SizesController : Controller {

        private readonly IApiResponse ApiResponse;
        private readonly IGetSizes GetSizesUseCase;

        public SizesController(IApiResponse apiResponse, IGetSizes getSizesUseCase) {
            this.ApiResponse = apiResponse;
            this.GetSizesUseCase = getSizesUseCase;
        }

        [HttpGet]
        public IActionResult Get([FromServices] IGetSizesInput getSizesInput, [FromServices] IGetSizesOutput getSizesOutput) {
            this.GetSizesUseCase.Execute(getSizesInput, getSizesOutput);

            return this.Json(this.ApiResponse.GetResponse(getSizesOutput));
        }

    }

}