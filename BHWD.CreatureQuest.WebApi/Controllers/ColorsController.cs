﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BHWD.CreatureQuest.WebApi.Controllers {

    [Route("[controller]")]
    public class ColorsController : Controller {

        private readonly IGetColors GetColorsUseCase;
        private readonly IApiResponse ApiResponse;

        public ColorsController(IApiResponse apiResponse, IGetColors getColorsUseCase) {
            this.GetColorsUseCase = getColorsUseCase;
            this.ApiResponse = apiResponse;
        }

        [HttpGet]
        public IActionResult Get([FromServices] IGetColorsInput getColorsInput, [FromServices] IGetColorsOutput getColorsOutput) {
            this.GetColorsUseCase.Execute(getColorsInput, getColorsOutput);

            return this.Json(this.ApiResponse.GetResponse(getColorsOutput));
        }

    }

}