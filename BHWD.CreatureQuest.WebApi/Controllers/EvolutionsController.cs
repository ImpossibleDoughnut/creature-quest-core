﻿using System;
using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BHWD.CreatureQuest.WebApi.Controllers {

    [Route("[controller]")]
    public class EvolutionsController : Controller {

        private readonly IApiResponse ApiResponse;
        private readonly IGetEvolutions GetEvolutionsUseCase;

        public EvolutionsController(IApiResponse apiResponse, IGetEvolutions getEvolutionsUseCase) {
            this.ApiResponse = apiResponse;
            this.GetEvolutionsUseCase = getEvolutionsUseCase;
        }

        [HttpGet]
        public IActionResult Get(
            [FromServices] IGetEvolutionsInput getEvolutionsInput,
            [FromServices] IGetEvolutionsOutput getEvolutionsOutput,
            [FromQuery(Name = "rarity")] List<Guid> rarityIds,
            [FromQuery(Name = "size")] List<Guid> sizeIds) {
            getEvolutionsInput.RarityIds = rarityIds;
            getEvolutionsInput.SizeIds = sizeIds;

            this.GetEvolutionsUseCase.Execute(getEvolutionsInput, getEvolutionsOutput);

            return this.Json(this.ApiResponse.GetResponse(getEvolutionsOutput));
        }

    }

}