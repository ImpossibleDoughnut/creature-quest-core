﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BHWD.CreatureQuest.WebApi.Controllers {

    [Route("[controller]")]
    public class RaritiesController : Controller {

        private readonly IApiResponse ApiResponse;
        private readonly IGetRarities GetRaritiesUseCase;

        public RaritiesController(IApiResponse apiResponse, IGetRarities getRaritiesUseCase) {
            this.ApiResponse = apiResponse;
            this.GetRaritiesUseCase = getRaritiesUseCase;
        }

        [HttpGet]
        public IActionResult Get([FromServices] IGetRaritiesInput getRaritiesInput, [FromServices] IGetRaritiesOutput getRaritiesOutput) {
            this.GetRaritiesUseCase.Execute(getRaritiesInput, getRaritiesOutput);

            return this.Json(this.ApiResponse.GetResponse(getRaritiesOutput));
        }

    }

}