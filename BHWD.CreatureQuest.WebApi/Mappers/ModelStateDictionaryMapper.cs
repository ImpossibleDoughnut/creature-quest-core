﻿using System.Collections.Generic;
using System.Linq;
using BHWD.CreatureQuest.WebApi.Contracts.Mappers;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BHWD.CreatureQuest.WebApi.Mappers {

    public class ModelStateDictionaryMapper : IMapper<ModelStateDictionary, List<string>> {

        public List<string> MapOne(ModelStateDictionary input, List<string> output) {
            if (input != null) {
                var errorMessages = input.Where(x => x.Value.Errors.Count > 0).Select(x => x.Value.Errors.Select(y => y.ErrorMessage)).Select(x => x.FirstOrDefault()).ToList();

                output?.AddRange(errorMessages);
            }

            return output;
        }

        public List<List<string>> MapMany(IEnumerable<ModelStateDictionary> inputList, List<List<string>> outputList) {
            if (inputList != null) {
                foreach (var input in inputList) {
                    var output = new List<string>();
                    this.MapOne(input, output);

                    outputList?.Add(output);
                }
            }

            return outputList;
        }

    }

}