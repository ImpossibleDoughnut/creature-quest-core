﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace BHWD.CreatureQuest.WebApi.ModelBinders {

    public class InterfacesModelBinder : ComplexTypeModelBinder {

        public InterfacesModelBinder(IDictionary<ModelMetadata, IModelBinder> propertyBinder) : base(propertyBinder) {
        }

        protected override object CreateModel(ModelBindingContext bindingContext) {
            return bindingContext.HttpContext.RequestServices.GetService(bindingContext.ModelType);
        }

    }

}