﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BHWD.CreatureQuest.WebApi.ModelBinders {

    public class InterfacesModelBinderProvider : IModelBinderProvider {

        public IModelBinder GetBinder(ModelBinderProviderContext context) {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (!context.Metadata.IsCollectionType && (context.Metadata.ModelType.GetTypeInfo().IsInterface || context.Metadata.ModelType.GetTypeInfo().IsAbstract)
                                                   && (context.BindingInfo.BindingSource == null || !context.BindingInfo.BindingSource.CanAcceptDataFrom(BindingSource.Services))) {
                var propertyBinders = new Dictionary<ModelMetadata, IModelBinder>();

                foreach (var property in context.Metadata.Properties) {
                    propertyBinders.Add(property, context.CreateBinder(property));
                }

                return new InterfacesModelBinder(propertyBinders);
            }

            return null;
        }

    }

}