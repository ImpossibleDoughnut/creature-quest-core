﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Controllers;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Controllers {

    [TestFixture]
    internal class AttacksControllerTest {

        private Mock<IApiResponse> ApiResponse;
        private AttacksController AttacksController { get; set; }
        private Mock<IGetAttacks> UseCase { get; set; }

        [SetUp]
        public void SetUp() {
            this.ApiResponse = new Mock<IApiResponse>();
            this.UseCase = new Mock<IGetAttacks>();
            this.AttacksController = new AttacksController(this.ApiResponse.Object, this.UseCase.Object);
        }

        [Test]
        public void GetExecutesUseCase() {
            var useCaseInput = new Mock<IGetAttacksInput>();
            var useCaseOutput = new Mock<IGetAttacksOutput>();

            this.AttacksController.Get(useCaseInput.Object, useCaseOutput.Object);

            this.UseCase.Verify(x => x.Execute(It.IsAny<IGetAttacksInput>(), It.IsAny<IGetAttacksOutput>()), Times.Once);
        }

        [Test]
        public void GetReturnsOutput() {
            var useCaseInput = new Mock<IGetAttacksInput>();
            var useCaseOutput = new Mock<IGetAttacksOutput>();

            var output = this.AttacksController.Get(useCaseInput.Object, useCaseOutput.Object);

            Assert.That(output, Is.Not.Null);
        }

    }

}