﻿using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Controllers;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.Controllers {

    [TestFixture]
    internal class RaritiesControllerTest {

        private Mock<IApiResponse> ApiResponse;
        private RaritiesController RaritiesController { get; set; }
        private Mock<IGetRarities> UseCase { get; set; }

        [SetUp]
        public void SetUp() {
            this.ApiResponse = new Mock<IApiResponse>();
            this.UseCase = new Mock<IGetRarities>();
            this.RaritiesController = new RaritiesController(this.ApiResponse.Object, this.UseCase.Object);
        }

        [Test]
        public void GetExecutesUseCase() {
            var useCaseInput = new Mock<IGetRaritiesInput>();
            var useCaseOutput = new Mock<IGetRaritiesOutput>();

            this.RaritiesController.Get(useCaseInput.Object, useCaseOutput.Object);

            this.UseCase.Verify(x => x.Execute(It.IsAny<IGetRaritiesInput>(), It.IsAny<IGetRaritiesOutput>()), Times.Once);
        }

        [Test]
        public void GetReturnsOutput() {
            var useCaseInput = new Mock<IGetRaritiesInput>();
            var useCaseOutput = new Mock<IGetRaritiesOutput>();
            var output = this.RaritiesController.Get(useCaseInput.Object, useCaseOutput.Object);

            Assert.That(output, Is.Not.Null);
        }

    }

}