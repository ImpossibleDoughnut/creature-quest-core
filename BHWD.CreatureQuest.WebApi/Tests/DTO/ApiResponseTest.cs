﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Contracts.Mappers;
using BHWD.CreatureQuest.WebApi.DTO;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Moq;
using NUnit.Framework;

namespace BHWD.CreatureQuest.WebApi.Tests.DTO {

    [TestFixture]
    internal class ApiResponseTest {

        private IApiResponse ApiResponse;
        private Mock<IUseCaseOutput> UseCaseOutput;
        private Mock<ModelStateDictionary> ModelStateDictionary;
        private Mock<IMapper<ModelStateDictionary, List<string>>> ModelStateDictionaryMapper;

        [SetUp]
        public void SetUp() {
            this.ModelStateDictionaryMapper = new Mock<IMapper<ModelStateDictionary, List<string>>>();
            this.UseCaseOutput = new Mock<IUseCaseOutput>();
            this.ModelStateDictionary = new Mock<ModelStateDictionary>();
            this.ApiResponse = new ApiResponse(this.ModelStateDictionaryMapper.Object);
        }

        [Test]
        public void ExecutesMapperObjectWhenModelStateDictionaryIsProvided() {
            this.ModelStateDictionaryMapper.Setup(x => x.MapOne(this.ModelStateDictionary.Object, It.IsAny<List<string>>())).Returns(new List<string>());
            this.ApiResponse.GetResponse(this.UseCaseOutput.Object, this.ModelStateDictionary.Object);

            this.ModelStateDictionaryMapper.Verify(x => x.MapOne(this.ModelStateDictionary.Object, It.IsAny<List<string>>()), Times.Once);
        }

        [Test]
        public void DoesNotExecutesMapperObjectWhenModelStateDictionaryIsNotProvided() {
            this.ApiResponse.GetResponse(this.UseCaseOutput.Object);

            this.ModelStateDictionaryMapper.Verify(x => x.MapOne(this.ModelStateDictionary.Object, It.IsAny<List<string>>()), Times.Never);
        }

        [Test]
        public void IncludesDataInGetResponseCall() {
            this.ApiResponse.GetResponse(this.UseCaseOutput.Object).TryGetValue(WebApi.DTO.ApiResponse.ResponseKeyData, out var actual);
            var expected = this.UseCaseOutput.Object;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void IncludesErrorsInGetResponseCall() {
            var errors = new List<string> {
                "Error One",
                "Error Two",
                "Error Three"
            };

            this.ModelStateDictionaryMapper.Setup(x => x.MapOne(this.ModelStateDictionary.Object, It.IsAny<List<string>>())).Returns(errors);
            this.ApiResponse.GetResponse(this.UseCaseOutput.Object, this.ModelStateDictionary.Object).TryGetValue(WebApi.DTO.ApiResponse.ResponseKeyErrors, out var actual);

            var expected = errors;

            Assert.AreEqual(expected, actual);
        }

    }

}