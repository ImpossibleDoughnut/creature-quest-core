﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.UseCase.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Contracts.Mappers;
using BHWD.CreatureQuest.WebApi.Controllers;
using BHWD.CreatureQuest.WebApi.DTO;
using BHWD.CreatureQuest.WebApi.DTO.UseCases;
using BHWD.CreatureQuest.WebApi.Mappers;
using BHWD.CreatureQuest.WebApi.ModelBinders;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BHWD.CreatureQuest.WebApi {

    public class Startup {

        public Startup(IConfiguration configuration) {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            var useCaseContainer = UseCaseContainer.Instance;

            services.AddSingleton<AttacksController, AttacksController>();
            services.AddSingleton<ColorsController, ColorsController>();
            services.AddSingleton<CreaturesController, CreaturesController>();
            services.AddSingleton<EvolutionsController, EvolutionsController>();
            services.AddSingleton<LevelsController, LevelsController>();
            services.AddSingleton<RaritiesController, RaritiesController>();
            services.AddSingleton<SizesController, SizesController>();

            services.AddMvc(o => { o.ModelBinderProviders.Insert(0, new InterfacesModelBinderProvider()); }).AddControllersAsServices();

            services.AddSingleton<IMapper<ModelStateDictionary, List<string>>, ModelStateDictionaryMapper>();
            services.AddSingleton<IApiResponse, ApiResponse>();

            services.AddSingleton(useCaseContainer.Get<ICreateCreature, ICreateCreatureInput, ICreateCreatureOutput>());
            services.AddTransient<ICreateCreatureInput, CreateCreatureInput>();
            services.AddTransient<ICreateCreatureOutput, CreateCreatureOutput>();

            services.AddSingleton(useCaseContainer.Get<IGetAttacks, IGetAttacksInput, IGetAttacksOutput>());
            services.AddTransient<IGetAttacksInput, GetAttacksInput>();
            services.AddTransient<IGetAttacksOutput, GetAttacksOutput>();

            services.AddSingleton(useCaseContainer.Get<IGetColors, IGetColorsInput, IGetColorsOutput>());
            services.AddTransient<IGetColorsInput, GetColorsInput>();
            services.AddTransient<IGetColorsOutput, GetColorsOutput>();

            services.AddSingleton(useCaseContainer.Get<IGetCreatures, IGetCreaturesInput, IGetCreaturesOutput>());
            services.AddTransient<IGetCreaturesInput, GetCreaturesInput>();
            services.AddTransient<IGetCreaturesOutput, GetCreaturesOutput>();

            services.AddSingleton(useCaseContainer.Get<IGetEvolutions, IGetEvolutionsInput, IGetEvolutionsOutput>());
            services.AddTransient<IGetEvolutionsInput, GetEvolutionsInput>();
            services.AddTransient<IGetEvolutionsOutput, GetEvolutionsOutput>();

            services.AddSingleton(useCaseContainer.Get<IGetLevels, IGetLevelsInput, IGetLevelsOutput>());
            services.AddTransient<IGetLevelsInput, GetLevelsInput>();
            services.AddTransient<IGetLevelsOutput, GetLevelsOutput>();

            services.AddSingleton(useCaseContainer.Get<IGetRarities, IGetRaritiesInput, IGetRaritiesOutput>());
            services.AddTransient<IGetRaritiesInput, GetRaritiesInput>();
            services.AddTransient<IGetRaritiesOutput, GetRaritiesOutput>();

            services.AddSingleton(useCaseContainer.Get<IGetSizes, IGetSizesInput, IGetSizesOutput>());
            services.AddTransient<IGetSizesInput, GetSizesInput>();
            services.AddTransient<IGetSizesOutput, GetSizesOutput>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }

    }

}