﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetAttacksOutput : IGetAttacksOutput {

        public List<Dictionary<string, object>> Attacks { get; set; } = new List<Dictionary<string, object>>();

    }

}