﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetLevelsOutput : IGetLevelsOutput {

        public List<Dictionary<string, object>> Levels { get; set; } = new List<Dictionary<string, object>>();

    }

}