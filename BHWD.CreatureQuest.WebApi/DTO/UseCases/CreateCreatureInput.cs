﻿using System;
using System.ComponentModel.DataAnnotations;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class CreateCreatureInput : ICreateCreatureInput {

        [Required]
        public Guid? AttackId { get; set; }

        public ushort? AttackRating { get; set; }

        [Required]
        public Guid? ColorId { get; set; }

        public ushort? DefenseRating { get; set; }

        [Required]
        public Guid? EvolutionId { get; set; }

        public ushort? HealthPoints { get; set; }

        [Required]
        public Guid? LevelId { get; set; }

        [Required]
        public ushort? LuckRating { get; set; }

        public ushort? ManaPool { get; set; }

        [Required]
        public string Name { get; set; }

        public Guid? NextCreatureId { get; set; }
        public ushort? PowerRating { get; set; }
        public Guid? PreviousCreatureId { get; set; }

        [Required]
        public Guid? RarityId { get; set; }

        [Required]
        public Guid? SizeId { get; set; }

    }

}