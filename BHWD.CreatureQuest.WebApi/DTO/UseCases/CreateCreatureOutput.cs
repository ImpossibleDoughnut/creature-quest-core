﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class CreateCreatureOutput : ICreateCreatureOutput {

        public Dictionary<string, object> Creature { get; set; }

    }

}