﻿using System;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetLevelsInput : IGetLevelsInput {

        public Guid EvolutionId { get; set; }
        public Guid SizeId { get; set; }

    }

}