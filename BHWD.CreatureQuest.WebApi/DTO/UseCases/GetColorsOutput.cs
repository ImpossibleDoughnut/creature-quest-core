﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetColorsOutput : IGetColorsOutput {

        public List<Dictionary<string, object>> Colors { get; set; } = new List<Dictionary<string, object>>();

    }

}