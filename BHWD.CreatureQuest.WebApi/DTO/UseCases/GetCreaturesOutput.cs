﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;

namespace BHWD.CreatureQuest.WebApi.DTO.UseCases {

    public class GetCreaturesOutput : IGetCreaturesOutput {

        public List<Dictionary<string, object>> Creatures { get; set; } = new List<Dictionary<string, object>>();

    }

}