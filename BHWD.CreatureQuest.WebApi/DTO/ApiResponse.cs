﻿using System.Collections.Generic;
using BHWD.CreatureQuest.UseCase.Contracts.UseCases;
using BHWD.CreatureQuest.WebApi.Contracts.DTO;
using BHWD.CreatureQuest.WebApi.Contracts.Mappers;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BHWD.CreatureQuest.WebApi.DTO {

    public class ApiResponse : IApiResponse {

        public const string ResponseKeyData = "data";
        public const string ResponseKeyErrors = "errors";

        private readonly IMapper<ModelStateDictionary, List<string>> ModelStateDictionaryMapper;

        public ApiResponse(IMapper<ModelStateDictionary, List<string>> modelStateDictionaryMapper) {
            this.ModelStateDictionaryMapper = modelStateDictionaryMapper;
        }

        public Dictionary<string, object> GetResponse(IUseCaseOutput useCaseOutput) {
            return this.GetResponse(useCaseOutput, null);
        }

        public Dictionary<string, object> GetResponse(IUseCaseOutput useCaseOutput, ModelStateDictionary modelStateDictionary) {
            var dictionary = new Dictionary<string, object>();
            var errors = new List<string>();

            if (modelStateDictionary != null) {
                errors = this.ModelStateDictionaryMapper.MapOne(modelStateDictionary, errors);
            }

            if (errors.Count > 0) {
                dictionary.Add(ResponseKeyErrors, errors);
            } else {
                dictionary.Add(ResponseKeyData, useCaseOutput);
            }

            return dictionary;
        }

    }

}