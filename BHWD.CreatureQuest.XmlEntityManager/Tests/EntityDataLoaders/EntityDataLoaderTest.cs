﻿using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataLoaders {

    [TestFixture]
    internal class EntityDataLoaderTest {

        private IEntityDataLoader<IDummyEntity> EntityDataLoader { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "test-creatures.xml";
            const string rootNode = "creatures";
            var resourceLoader = new XmlResourceLoader();
            var entityDataMapper = new DummyEntityDataMapper();
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityFactory = new EntityFactory();

            EntityFactory.Instance = entityFactory;
            entityFactory.Register<IDummyEntity>(typeof(DummyEntity));

            this.EntityDataLoader = new EntityDataLoader<IDummyEntity, XPathDocument, XPathNavigator>(
                resourceLoader,
                entityDataLoaderIterator,
                entityDataMapper,
                resourceName,
                rootNode);
        }

        [Test]
        public void LoadEntityData() {
            const int expected = 6;
            var actual = this.EntityDataLoader.Execute().Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}