﻿using System.Xml.XPath;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.ResourceLoaders;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.ResourceLoaders {

    [TestFixture]
    internal class XmlResourceLoaderTest {

        private string ResourceName { get; set; }
        private IResourceLoader<XPathDocument> ResourceLoader { get; set; }

        [SetUp]
        public void SetUp() {
            this.ResourceName = "test-creatures.xml";
            this.ResourceLoader = new XmlResourceLoader();
        }

        [Test]
        public void ExecuteReturnsData() {
            var actual = this.ResourceLoader.Execute(this.ResourceName);

            Assert.IsNotNull(actual);
        }

        [Test]
        public void ExecuteReturnsNothing() {
            var actual = this.ResourceLoader.Execute("does not exist");

            Assert.IsNull(actual);
        }

    }

}