﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataLoaderMappers {

    [TestFixture]
    internal class ColorDataLoaderMapperTest {

        private IEntityDataMapper<XPathNavigator, IColor> EntityDataMapper { get; set; }
        private List<XPathNavigator> XPathNavigators { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "colors.xml";
            const string rootNode = "colors";

            var xmlResourceLoader = new XmlResourceLoader();
            var xPathDocument = xmlResourceLoader.Execute(resourceName);
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityFactory = new EntityFactory();

            EntityFactory.Instance = entityFactory;
            entityFactory.Register<IColor>(typeof(Color));

            this.XPathNavigators = entityDataLoaderIterator.GetList(xPathDocument, rootNode);
            this.EntityDataMapper = new ColorDataMapper();
        }

        [Test]
        public void MapName() {
            var color = new Color();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(ColorDataMapper.SourceKeyName, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, color);

            var actual = color.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapMany() {
            var colors = new List<IColor>();
            var expected = this.XPathNavigators.Count;

            this.EntityDataMapper.MapMany(this.XPathNavigators, colors);

            var actual = colors.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}