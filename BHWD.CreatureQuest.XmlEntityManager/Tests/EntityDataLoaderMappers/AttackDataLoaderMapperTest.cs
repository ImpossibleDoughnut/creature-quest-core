﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataLoaderMappers {

    [TestFixture]
    internal class AttackDataLoaderMapperTest {

        private IEntityDataMapper<XPathNavigator, IAttack> EntityDataMapper { get; set; }
        private List<XPathNavigator> XPathNavigators { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "attacks.xml";
            const string rootNode = "attacks";

            var xmlResourceLoader = new XmlResourceLoader();
            var xPathDocument = xmlResourceLoader.Execute(resourceName);
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityFactory = new EntityFactory();

            EntityFactory.Instance = entityFactory;
            entityFactory.Register<IAttack>(typeof(Attack));

            this.XPathNavigators = entityDataLoaderIterator.GetList(xPathDocument, rootNode);
            this.EntityDataMapper = new AttackDataMapper();
        }

        [Test]
        public void MapName() {
            var attack = new Attack();
            var xPathNavigator = this.XPathNavigators.FirstOrDefault();
            var expected = xPathNavigator?.GetAttribute(AttackDataMapper.SourceKeyName, xPathNavigator.NamespaceURI);

            this.EntityDataMapper.MapOne(xPathNavigator, attack);

            var actual = attack.Name;

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MapMany() {
            var attacks = new List<IAttack>();
            var expected = this.XPathNavigators.Count;

            this.EntityDataMapper.MapMany(this.XPathNavigators, attacks);

            var actual = attacks.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}