﻿using System.Xml.XPath;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSetHydrators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataSetHydrators;
using BHWD.CreatureQuest.XmlEntityManager.EntityDataSets;
using BHWD.CreatureQuest.XmlEntityManager.ResourceLoaders;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityDataSetHydrators {

    [TestFixture]
    internal class EntityDataSetHydratorTest {

        private IEntityDataLoader<IDummyEntity> EntityDataLoader { get; set; }
        private IEntityDataSet<IDummyEntity> EntityDataSet { get; set; }
        private IEntityDataSetHydrator<IDummyEntity> EntityDataSetHydrator { get; set; }

        [SetUp]
        public void SetUp() {
            const string resourceName = "test-creatures.xml";
            const string rootNode = "creatures";
            var resourceLoader = new XmlResourceLoader();
            var entityDataMapper = new DummyEntityDataMapper();
            var entityDataLoaderIterator = new EntityDataLoaderIterator();
            var entityFactory = new EntityFactory();

            EntityFactory.Instance = entityFactory;
            entityFactory.Register<IDummyEntity>(typeof(DummyEntity));

            this.EntityDataLoader = new EntityDataLoader<IDummyEntity, XPathDocument, XPathNavigator>(
                resourceLoader,
                entityDataLoaderIterator,
                entityDataMapper,
                resourceName,
                rootNode);
            this.EntityDataSet = new EntityDataSet<IDummyEntity>();
            this.EntityDataSetHydrator = new EntityDataSetHydrator<IDummyEntity>();
        }

        [Test]
        public void Execute() {
            const int expected = 6;

            this.EntityDataSetHydrator.Execute(this.EntityDataLoader, this.EntityDataSet);

            var actual = this.EntityDataSet.Entities.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}