﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityGateways {

    [TestFixture]
    internal class GetAllCreaturesGatewayTest {

        private List<ICreature> Creatures { get; set; }
        private IGetManyEntityGateway<ICreature> GetAllCreaturesGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Creatures = new List<ICreature> {
                new Creature(),
                new Creature(),
                new Creature()
            };
            var entityDataSet = new StubEntityDataSet<ICreature>(this.Creatures);

            this.GetAllCreaturesGateway = new GetAllCreaturesGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetAllCreaturesGateway.GetAll().Count;
            var expected = this.Creatures.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}