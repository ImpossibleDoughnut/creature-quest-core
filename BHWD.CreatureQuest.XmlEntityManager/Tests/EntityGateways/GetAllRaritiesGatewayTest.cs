﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Tests.TestDoubles;
using NUnit.Framework;

namespace BHWD.CreatureQuest.XmlEntityManager.Tests.EntityGateways {

    [TestFixture]
    internal class GetAllRaritiesGatewayTest {

        private List<IRarity> Rarities { get; set; }
        private IGetManyEntityGateway<IRarity> GetAllRaritiesGateway { get; set; }

        [SetUp]
        public void SetUp() {
            this.Rarities = new List<IRarity> {
                new Rarity(),
                new Rarity(),
                new Rarity()
            };
            var entityDataSet = new StubEntityDataSet<IRarity>(this.Rarities);

            this.GetAllRaritiesGateway = new GetAllRaritiesGateway(entityDataSet);
        }

        [Test]
        public void Executes() {
            var actual = this.GetAllRaritiesGateway.GetAll().Count;
            var expected = this.Rarities.Count;

            Assert.That(actual, Is.EqualTo(expected));
        }

    }

}