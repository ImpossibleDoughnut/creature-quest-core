﻿using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSetHydrators {

    public interface IEntityDataSetHydrator<TEntity> where TEntity : IEntity {

        void Execute(IEntityDataLoader<TEntity> entityDataLoader, IEntityDataSet<TEntity> entityDataSet);

    }

}