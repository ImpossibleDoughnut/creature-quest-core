﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaders {

    public interface IEntityDataLoader<TEntity> where TEntity : IEntity {

        List<TEntity> Execute();

    }

}