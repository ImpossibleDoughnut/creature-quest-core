﻿using System.Collections.Generic;

namespace BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaderIterators {

    public interface IEntityDataLoaderIterator<TInput, TOutput> {

        List<TOutput> GetList(TInput input, string rootNode);

    }

}