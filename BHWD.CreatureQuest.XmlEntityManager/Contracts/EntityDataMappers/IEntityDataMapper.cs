﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;

namespace BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers {

    public interface IEntityDataMapper<TSource, TDestination> where TDestination : IEntity {

        void MapOne(TSource source, TDestination destination);
        void MapMany(List<TSource> sources, List<TDestination> destinations);

    }

}