﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaderIterators;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataLoaders;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.ResourceLoaders;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataLoaders {

    public class EntityDataLoader<TEntity, TResourceLoaderOutput, TEntityDataMapperSource> : IEntityDataLoader<TEntity> where TEntity : IEntity {

        private readonly IResourceLoader<TResourceLoaderOutput> ResourceLoader;
        private readonly IEntityDataLoaderIterator<TResourceLoaderOutput, TEntityDataMapperSource> EntityDataLoaderIterator;
        private readonly IEntityDataMapper<TEntityDataMapperSource, TEntity> EntityDataMapper;
        private readonly string ResourceName;
        private readonly string RootNode;

        public EntityDataLoader(
            IResourceLoader<TResourceLoaderOutput> resourceLoader,
            IEntityDataLoaderIterator<TResourceLoaderOutput, TEntityDataMapperSource> entityDataLoaderIterator,
            IEntityDataMapper<TEntityDataMapperSource, TEntity> entityDataMapper,
            string resourceName,
            string rootNode) {
            this.ResourceLoader = resourceLoader;
            this.EntityDataLoaderIterator = entityDataLoaderIterator;
            this.EntityDataMapper = entityDataMapper;
            this.ResourceName = resourceName;
            this.RootNode = rootNode;
        }

        public List<TEntity> Execute() {
            var entities = new List<TEntity>();
            var resourceLoaderOutput = this.ResourceLoader.Execute(this.ResourceName);
            var entityDataLoaderIterator = this.EntityDataLoaderIterator.GetList(resourceLoaderOutput, this.RootNode);
            var entityFactory = EntityFactory.Instance;

            foreach (var entityData in entityDataLoaderIterator) {
                var entity = entityFactory.Create<TEntity>();

                this.EntityDataMapper.MapOne(entityData, entity);

                entities.Add(entity);
            }

            return entities;
        }

    }

}