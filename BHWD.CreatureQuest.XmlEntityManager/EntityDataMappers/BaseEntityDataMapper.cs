﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Entities;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataMappers;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityDataMappers {

    public abstract class BaseEntityDataMapper<TSource, TDestination> : IEntityDataMapper<TSource, TDestination> where TDestination : IEntity {

        public abstract void MapOne(TSource source, TDestination destination);

        public void MapMany(List<TSource> sources, List<TDestination> destinations) {
            var entityFactory = EntityFactory.Instance;

            foreach (var source in sources) {
                var destination = entityFactory.Create<TDestination>();

                this.MapOne(source, destination);

                destinations.Add(destination);
            }
        }

    }

}