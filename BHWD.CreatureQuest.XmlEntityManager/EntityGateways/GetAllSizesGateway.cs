﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityGateways {

    public class GetAllSizesGateway : IGetManyEntityGateway<ISize> {

        private readonly IEntityDataSet<ISize> EntityDataSet;

        public GetAllSizesGateway(IEntityDataSet<ISize> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<ISize> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}