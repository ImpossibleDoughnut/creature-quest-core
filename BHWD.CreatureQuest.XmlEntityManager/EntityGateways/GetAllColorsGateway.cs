﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityGateways {

    public class GetAllColorsGateway : IGetManyEntityGateway<IColor> {

        private readonly IEntityDataSet<IColor> EntityDataSet;

        public GetAllColorsGateway(IEntityDataSet<IColor> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<IColor> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}