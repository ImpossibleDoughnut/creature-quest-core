﻿using System.Collections.Generic;
using BHWD.CreatureQuest.Entity.Contracts.Entities;
using BHWD.CreatureQuest.Entity.Contracts.EntityGateways;
using BHWD.CreatureQuest.XmlEntityManager.Contracts.EntityDataSet;

namespace BHWD.CreatureQuest.XmlEntityManager.EntityGateways {

    public class GetAllAttacksGateway : IGetManyEntityGateway<IAttack> {

        private readonly IEntityDataSet<IAttack> EntityDataSet;

        public GetAllAttacksGateway(IEntityDataSet<IAttack> entityDataSet) {
            this.EntityDataSet = entityDataSet;
        }

        public List<IAttack> GetAll() {
            return this.EntityDataSet.Entities;
        }

    }

}